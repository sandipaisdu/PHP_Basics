<!DOCTYPE html>
<html>
<head>
<title>This is a Title.</title>
</head>
<body>
   <?php
   echo "Hello World";
   //...more code
   echo"Last Statements";
   //scripting ends here with no PHP closing tag
   ?>
   <?php
     echo"My Name is Khan";
	 //instruction separation by (;)
	 echo"Dilwaley Dulhaniya Ley Jayngey";
   ?>
   <?php 
   //initializing one line comment
   echo"This is a comment";
   /* This is a
   multiline comment*/
   #this is a single line comment
   ?>
</body>
</html>